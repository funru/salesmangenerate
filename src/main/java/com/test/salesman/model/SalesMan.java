package com.test.salesman.model;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SalesMan {
    private List<Integer> path;
    private int fullDistance;

    public SalesMan(int countCities){
        this.path = IntStream.range(0,countCities).boxed().collect(Collectors.toList());
        this.fullDistance = 0;
    }

    public List<Integer> getPath() {
        return path;
    }

    public void setPath(List<Integer> path) {
        this.path = path;
    }

    public int getFullDistance() {
        return fullDistance;
    }

    public void shufflePath(){
        Collections.shuffle(this.path);
    }

    public void calculateFullDistance(Integer[][] mapCities){
        Integer resultDistance = 0;
        for (int i = 0; i < this.path.size()-1; i++){
            resultDistance += mapCities[this.path.get(i)][this.path.get(i+1)];
        }
        this.fullDistance = resultDistance;
    }
}
