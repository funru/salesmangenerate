package com.test.salesman.model;

public class City {

    private final int x;
    private final int y;

    public City() {
        this.x = (int) (Math.random() *  1000);
        this.y = (int) (Math.random() *  1000);
    }

    private int getX() {
        return x;
    }

    private int getY() {
        return y;
    }

    public int distanceToCity(City city) {
        int x = Math.abs(getX() - city.getX());
        int y = Math.abs(getY() - city.getY());
        return (int)Math.round(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)));
    }
}
