package com.test.salesman;

import com.test.salesman.model.City;
import com.test.salesman.model.SalesMan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Main {

    private static final int COUNT_OF_BEST = 50;
    private static final int countCities = 10;

    public static void main(String[] args) {
        Integer[][] distanceCities  = new Integer[countCities][countCities];
        City[] cities = new City[countCities];
        for (int z = 0; z < countCities; z++){
            cities[z] = new City();
        }
        for (int x =0;x<countCities; x++){
            for (int y=0; y<countCities; y++){
                if(x==y){
                    distanceCities[x][y] = 0;
                } else if (x < y){
                    distanceCities[x][y] = cities[x].distanceToCity(cities[y]);
                } else {
                    distanceCities[x][y] = distanceCities[y][x];
                }
            }
        }
        System.out.println("Map of distance: ");
        for (int x =0;x<countCities; x++){
            for (int y=0; y<countCities; y++){
                System.out.print(distanceCities[x][y] + " ");
            }
            System.out.println("---");
        }
        //first generation
        List<SalesMan> salesMEN = new ArrayList<>();
        for (int s = 0; s < 1000; s++){
            SalesMan salesMan = new SalesMan(countCities);
            salesMan.shufflePath();
            salesMan.calculateFullDistance(distanceCities);
            salesMEN.add(salesMan);
        }
        salesMEN = getBestSalesMan(salesMEN);
        int countWithoutProgress = 0;
        int best = salesMEN.get(0).getFullDistance();
        while(countWithoutProgress < 3){
            generateNext(salesMEN,distanceCities);
            salesMEN = getBestSalesMan(salesMEN);
            int generationBest = salesMEN.get(0).getFullDistance();
            if(best > generationBest){
                countWithoutProgress = 0;
                best = generationBest;
            } else {
                countWithoutProgress++;
            }
        }
        SalesMan bestSale = getBestSalesMan(salesMEN).get(0);
        System.out.println("Best distance: ");
        System.out.println(bestSale.getFullDistance());
        System.out.println("Best path: ");
        System.out.println(bestSale.getPath().toString());
    }

    private static List<SalesMan> getBestSalesMan(List<SalesMan> salesMEN){
        salesMEN.sort(Comparator.comparing(SalesMan::getFullDistance));
        return salesMEN.subList(0,COUNT_OF_BEST);
    }

    private static void generateNext(List<SalesMan> previousGeneration, Integer[][] distanceCities){
        while (previousGeneration.size() < 1000){
            int parent = (int)(Math.random() * COUNT_OF_BEST);
            int startSub = (int)(Math.random() * (countCities-1));
            int finalSub = (int)(Math.random() * (countCities - startSub)) + startSub;
            List<Integer> pathParent = previousGeneration.get(parent).getPath();
            List <Integer> crossPath = new ArrayList<>(pathParent.subList(startSub,finalSub));
            if(crossPath.size() > 0) {
                Collections.shuffle(crossPath);
                List<Integer> childPath = new ArrayList<>();
                childPath.addAll(new ArrayList<>(pathParent.subList(0, startSub)));
                childPath.addAll(crossPath);
                childPath.addAll(new ArrayList<>(pathParent.subList(finalSub, pathParent.size())));
                SalesMan childSalesMan = new SalesMan(countCities);
                childSalesMan.setPath(childPath);
                childSalesMan.calculateFullDistance(distanceCities);
                previousGeneration.add(childSalesMan);
            }
        }
    }
}
